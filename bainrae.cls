%% This is file `bainrae.cls'
%% 
%% This file is a very basic reproduction of the ppt template provided in the 
%% official Charte Graphique INRAE Version 3 (septembre 2020) and can be used
%% by any INRAE staff for her professional communications.
%%
%% It is meant to be used with pdftex, and is provided without any guarantee. 
%% It has been set up by Nathalie Vialaneix <nathalie.vialaneix@inrae.fr> with
%% the help of Thomas Schiex for image vectorization. Feel free to improve it.
%%
%% Licence: WTFPL version 2 compatible LPRAB version 1
%% Logo, images and used Charte graphique remain the property of INRAE. Only 
%% the LaTeX template is distributed on my behalf under the abovementionned
%% licence
%% Copyright (C) 2021 Nathalie Vialaneix
%%
%% 14. March 2021: initial version of the class (v0.1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Header
\ProvidesClass{bainrae}[2021/03/14 v0.1 bainrae beamer class]
\NeedsTeXFormat{LaTeX2e}[1995/06/01]
\LoadClass{beamer}

%% Required packages
\usepackage{graphicx}

%% Colors (as recommanded in Charte graphique INRAE
\definecolor{inraeg}{HTML}{00a3a6}
\definecolor{inraedg}{HTML}{008c8e}
\definecolor{newred}{HTML}{ed6e6c}

%% Custom settings
\hypersetup{colorlinks=true,linkcolor=inraedg,urlcolor=newred}
%%% remove beamer navigation
\beamertemplatenavigationsymbolsempty\DeclareGraphicsExtensions{.pdf,.jpg,.png,
.jpeg}
\usecolortheme[named=inraedg]{structure}
\bibliographystyle{apalike}

\setbeamertemplate{footline}{%
	\linethickness{0.25pt}
  \begin{beamercolorbox}[leftskip=.3cm,wd=\paperwidth,ht=0.05\paperwidth,
  sep=0cm]{Location bar}
    \usebeamerfont{section in head/foot}%
    \begin{minipage}{2 cm}
      \hspace*{-0.372cm}
      \includegraphics[height=1cm]{INRAE_AE_withtransp.pdf}~%
    \end{minipage}
    \begin{minipage}{6 cm}
      \includegraphics[height=0.2cm]{INRAE}\\
      \insertshorttitle\\
      \insertshortdate~/ \insertshortauthor
    \end{minipage}
    \hfill
    \begin{minipage}{1 cm}
      \highlighton{p.~\insertframenumber}
    \end{minipage}
  \end{beamercolorbox}
}

\setbeamertemplate{frametitle}{\includegraphics[width=0.4cm]{bullet_head.pdf}
\hspace*{0.2cm} \insertframetitle \par}

\setbeamertemplate{title}{\includegraphics[width=0.4cm]{bullet.pdf}
\hspace*{0.2cm} bla \insertitle \par}

